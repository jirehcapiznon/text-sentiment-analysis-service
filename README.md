# Text Sentiment Analysis Service

## Description

This plugin connects your Reekoh Instance to Microsoft Text Analytics API to detect sentiment from text input.

### Configuration

The Text Sentiment Analysis Service Plugin configuration can be done once you've created your own pipeline in Reekoh. To configure the plugin, you will be asked to provide the following details:

- **Service Name** - This is a label given to your plugin to locate it easily in your pipeline.
- **API Key** - Microsoft Text Analytics API key.
- **API Endpoint** - Microsoft Text Analytics API Endpoint.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/service/text-sentiment-analysis-service/1.0.0/text-sentiment-config.png)

## Send Data

In order to simulate sending device data, You will need a Gateway Plugin to send the device data to Text Sentiment Analysis Plugin and a Connector/Storage Plugin to receive the output of your data. In the screenshot below, it uses HTTP Gateway to send input data and Webhooks Connector to view the output. Note: Look for each documentation on how to use these plugins.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/service/text-sentiment-analysis-service/1.0.0/text-sentiment-pipeline.png)

Make sure your plugins and pipeline are successfully deployed.
 
Using **POST MAN** as HTTP Client simulator, you can now simulate sending device data.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/service/text-sentiment-analysis-service/1.0.0/text-sentiment-postman.png)

## Data format

The HTTP Gateway Plugin accepts application/json, application/x-www-form-urlencoded and multipart/form-data formats. Also, a "device" field is required to be in the request body. This device field should contain a device ID which is registered in Reekoh's Device Registry.

```javascript
{ 
	"device": "jhondevice",
    "documents": [{
    	"language": "en",
        "id": "1",
        "text": "Hello world. This is some input text that I love."
    },    {
        "language": "fr",
        "id": "2",
        "text": "Bonjour tout le monde"
      }]
}
```

## Verify Data

The device data will be ingested by the HTTP Gateway plugin, which will be forwarded to all the other plugins that are connected to it in the pipeline.

To verify if the data is ingested properly in the pipeline, you need to check the **LOGS** tab in every plugin in the pipeline. All the data that have passed through the plugin will be logged in the **LOGS** tab.
If an error occurs, an error exception will be logged in the **EXCEPTIONS** tab.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/service/text-sentiment-analysis-service/1.0.0/text-sentiment-logs.png)

The data that was sent to HTTP Gateway will be forwarded to Text Analytics Service. Then after processing, 
the output will be forwarded to the Webhooks Connector. The output of the process should appear in RequestBin.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/service/text-sentiment-analysis-service/1.0.0/text-sentiment-verify.png)